﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revolut.DtoModels;
using Revolut.Model;
using Revolut.Services;

namespace Revolut.Controllers
{
    [Route("api/authentication")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private IUserService _userService;

        public AuthenticationController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{PhoneNumber}")]
            public IActionResult FindUser(string PhoneNumber)
        {
            var u = _userService.FindUser(PhoneNumber);

            if (u == null)
                return NotFound("No such user found");

            return Ok(u);
        }
        [HttpPost("register")]
        public IActionResult AddUser([FromBody] User user)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var u = _userService.AddUser(user);


            if (u == null)
                return NotFound("User with this CNP already exist");

            return Ok(u);
        }

        [HttpPut("update")]
        public IActionResult UpdateUser([FromBody] User user)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var u = _userService.UpdateUser(user);

            if (u == null)
                return NotFound($"There is no user with phone number {user.PhoneNumber}");

            return Ok(u);
        }

        [HttpDelete("{PhoneNumber}")]
        public IActionResult DeleteUser(string PhoneNumber)
        {
            try
            {
                var user = _userService.DeleteUser(PhoneNumber);
                return Ok(user);
            }
            catch (NullReferenceException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpPost("{PhoneNumber}/login")]
        public IActionResult CheckLogin(string PhoneNumber,[FromForm]string Password)
        {
            try
            {
                var check = _userService.CheckLogin(PhoneNumber, Password);
                return Ok(check);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        
    }
}