﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revolut.DtoModels;
using Revolut.Model;
using Revolut.Services;

namespace Revolut.Controllers
{
    [Route("api/friendgroup")]
    [ApiController]
    public class FriendGroupController : ControllerBase
    {
        private IFriendGroupService _FriendGroupService;

        public FriendGroupController(IFriendGroupService FriendGroupService)
        {
            _FriendGroupService = FriendGroupService;
        }

        [HttpPost("create")]
        public IActionResult CreateGroup([FromBody] FriendGroup newGroup)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var g = _FriendGroupService.CreateGroup(newGroup);

            if (g == null)
                return NotFound("This group already exists");

            return Ok(g);
        }
        [HttpPost("{GroupName}/addperson")]
        public IActionResult AddPerson(string GroupName, [FromForm] string PhoneNumber)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                _FriendGroupService.AddPerson(PhoneNumber, GroupName);

                return Ok();
            }
            catch (NullReferenceException ex)
            {
                return NotFound($"Could not find either group or user with  {PhoneNumber}, {GroupName}");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{GroupName}/removeperson")]
        public IActionResult RemovePerson(string GroupName, [FromForm] string PhoneNumber)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                _FriendGroupService.RemovePerson(PhoneNumber, GroupName);
                return Ok();
            }
            catch (NullReferenceException ex)
            {
                return NotFound($"Could not find either group or user with {GroupName}, {PhoneNumber}");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{GroupName}")]
        public IActionResult DeleteGroup(string GroupName)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _FriendGroupService.DeleteGroup(GroupName);
                return Ok("Group deleted successfully");
                }
            catch(Exception ex)
            {
                return BadRequest($"Group {GroupName} does not exist " + ex.Message);
            }
        }

        [HttpGet("{GroupName}")]
        public IActionResult GetAllMembers(string GroupName)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var g = _FriendGroupService.GetAllMembers(GroupName);

                return Ok(g);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet()]
        public IActionResult GetAllGroups()
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var groups = _FriendGroupService.GetAllGroups();

                return Ok(groups);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
