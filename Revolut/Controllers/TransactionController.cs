﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Revolut.DtoModels;
using Revolut.Model;
using Revolut.Services;

namespace Revolut.Controllers
{
    [Route("api/transaction")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private ITransferService _transferService;
        private IUserService _userService;

        public TransactionController(ITransferService transferService, IUserService userService)
        {
            _transferService = transferService;
            _userService = userService;
        }
         
        [HttpPost("transfer")]
        public IActionResult Transfer([FromBody] Transfer transfer)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var user1 = _userService.FindUser(transfer.PhoneNumber);
                var user2 = _userService.FindUser(transfer.TargetPhoneNumber); 
                var u = _transferService.TransferBalance(user1, user2, transfer.Amount);
                return Ok(u);
            }
            catch (NullReferenceException ex)
            {
                return NotFound($"One of the users with phone numbers {transfer.PhoneNumber} or {transfer.TargetPhoneNumber} does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{phoneNumber}")]
        public IActionResult GetBalance(string phoneNumber)
        {
            try
            {
                var user = _transferService.GetBalance(phoneNumber);

                return Ok(user.Balance);
            }
            catch (NullReferenceException ex)
            {
                return NotFound($"The user with phone number {phoneNumber} does not exist");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("{phoneNumber}/topup")]
        public IActionResult TopUp(string phoneNumber, [FromBody] Transfer transfer)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                var transferToUser = _userService.FindUser(phoneNumber);
                var user = _transferService.TopUp(transferToUser, transfer.Amount);

                return Ok(user);
            }
            catch(NullReferenceException ex)
            {
                return NotFound($"The user with phone number {phoneNumber} does not exist");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{phoneNumber}/transfers")]
        public IActionResult GetTransfers(string phoneNumber)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                var transfers = _transferService.GetTransfers(phoneNumber);

                return Ok(transfers);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}