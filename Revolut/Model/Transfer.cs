﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Revolut.Model
{
    public class Transfer
    {
        [Required]
        public string PhoneNumber { get; set; }
        public string TargetPhoneNumber { get; set; }
        public int Amount { get; set; }
        public String Date { get; set; }
    }
}
