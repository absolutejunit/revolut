﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Revolut.Model
{
    public class UsersGroup
    {
        [Required]
        public string GroupName;
        [Required]
        public string UserPhoneNumber;
    }
}
