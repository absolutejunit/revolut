﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Revolut.Model
{
    public class FriendGroup
    {
        [Required]
        [Key]
        public string OwnerPhoneNumber { get; set; }
        [Required]
        [MaxLength(30)]
        public string GroupName { get; set; }
    }
}
