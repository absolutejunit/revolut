﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Revolut.DtoModels
{
    public class FriendGroupDTO
    {
        [Required]
        [MaxLength(30)]
        public string GroupName { get; set; }
        [MaxLength(30)]
        public string OwnerPhoneNumber { get; set; }
    }
}
