﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Revolut.DtoModels
{
    public class UsersGroupDTO
    {
        [Required]
        public string UserPhoneNumber;
    }
}
