﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Revolut.DtoModels
{
    public class TransferDTO
    {
        [Required]
        public string PhoneNumber { get; set; }
        public string TargetPhoneNumber { get; set; }
        public String Date { get; set; }
    }
}
