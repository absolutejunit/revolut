﻿using System;
using Revolut.Model;
using System.Collections.Generic;
using System.Linq;
using Revolut.DtoModels;
using Revolut.Mapping;
using Revolut.Repositories;
using AutoMapper;

namespace Revolut.Services
{
    public class UsersService : IUserService
    {
        private IUserMapping _userMapping;
        private IMapper _mapper;
        private IUserRepository _userRepository;

        public UsersService(IUserMapping userMapping, IUserRepository userRepository, IMapper mapper)
        {
            _userMapping = userMapping;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public CreateUserDto AddUser(User user)
        {
            var checkUser = _userRepository.FindUser(user.PhoneNumber);
            var addUser = _mapper.Map<CreateUserDto>(user);

            if (checkUser == null)
            {
                var newUser = _userRepository.Add(addUser);
                return newUser;
            }
            else
                return null;
        }

        public DtoUser DeleteUser(string PhoneNumber)
        {
            var user = _userRepository.FindUser(PhoneNumber);

            if (user != null)
            {
               _userRepository.Delete(PhoneNumber);
                var deletedUser =_mapper.Map<DtoUser>(user);
                return deletedUser;
            }
            else
                return null;
        }

        public DtoUser UpdateUser(User user)
        {
            var u = _mapper.Map<DtoUser>(_userRepository.FindUser(user.PhoneNumber));

            if(u != null)
            {
                _userRepository.Update(u);
                return u;
            }
            else
            {
                return null;
            }
        }

        public DtoUser FindUser(string PhoneNumber)
        {
            var u = _userRepository.FindUser(PhoneNumber);

            if( u != null)
            {
                var foundUser = _mapper.Map<DtoUser>(u);

                return foundUser;
            }
            else
            {
                return null;
            }
        }

        public bool CheckLogin(string PhoneNumber, string Password)
        {
            var check = _userRepository.CheckLogin(PhoneNumber, Password);
            if (check != null)
                return true;
            else
                return false;

        }
    }
}
