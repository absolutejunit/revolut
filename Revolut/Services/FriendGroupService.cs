﻿using System;
using Revolut.Model;
using System.Collections.Generic;
using System.Linq;
using Revolut.DtoModels;
using Revolut.Mapping;
using Revolut.Repositories;
using AutoMapper;

namespace Revolut.Services
{
    public class FriendGroupService : IFriendGroupService
    {
        private IUserService _userService;
        private IFriendGroupRepository _friendGroupRepository;
        private IMapper _mapper;

        public FriendGroupService(IUserService userService, IFriendGroupRepository friendGroupRepository, IMapper mapper)
        {
            _userService = userService;
            _friendGroupRepository = friendGroupRepository;
            _mapper = mapper;
        }

        public FriendGroupDTO CreateGroup(FriendGroup group)
        {
            var gr = _friendGroupRepository.FindGroup(group.GroupName);
            var createGroup = _mapper.Map<FriendGroupDTO>(group);

            if(gr == null)
            {
                var newGroup = _friendGroupRepository.CreateGroup(createGroup);
                return newGroup;
            }
            else
            {
                throw new Exception("Group Already Exists");
            }
        }

        public IList<UsersGroupDTO> GetAllMembers(string GroupName)
        {
            var group = _friendGroupRepository.GetMembers(GroupName);

            if (group != null)
            {
                return group;
            }
            else
                return null;
        }
        

        public string DeleteGroup (string GroupName)
        {
            var gr = _friendGroupRepository.FindGroup(GroupName);

            if(gr != null)
            {
                return _friendGroupRepository.DeleteGroup(GroupName);
            }
            else
            {
                return "Group does not exist";
            }
        }

        public void AddPerson (string PhoneNumber, string GroupName)
        {
            bool exists = false;
            if(_friendGroupRepository.FindGroup(GroupName) != null)
            {
                exists = true;
            }
            _friendGroupRepository.AddPerson(PhoneNumber, GroupName, exists);
        }

        public void RemovePerson(string PhoneNumber, string GroupName)
        {
            _friendGroupRepository.RemovePerson(PhoneNumber, GroupName);
        }

        public FriendGroupDTO FindGroup(string GroupName)
        {
            return Mapper.Map<FriendGroupDTO>(_friendGroupRepository.FindGroup(GroupName));
        }

        public IList<FriendGroupDTO> GetAllGroups()
        {
            var groups = _friendGroupRepository.GetAllGroups();


            foreach(FriendGroupDTO group in groups)
            {
                var name = group.GroupName;

                var memberCount = _friendGroupRepository.GetAllMembers(name).Count() + 1;

                group.OwnerPhoneNumber = memberCount.ToString();
            }

            return groups;
        }
    }
}
