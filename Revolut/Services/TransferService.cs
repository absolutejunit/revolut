﻿using System;
using Revolut.Model;
using System.Collections.Generic;
using System.Linq;
using Revolut.DtoModels;
using Revolut.Mapping;
using Revolut.Repositories;

namespace Revolut.Services
{
    public class TransferService : ITransferService
    {

        private IUserService _userService;
        private ITransferRepository _transferRepository;
        private IUserRepository _userRepository;

        public TransferService(IUserService userService, ITransferRepository transferRepository, IUserRepository userRepository)
        {
            _userService = userService;
            _transferRepository = transferRepository;
            _userRepository = userRepository;
        }
        public DtoUser TransferBalance(DtoUser user1, DtoUser user2, int Amount)
        {
            var result = _transferRepository.TransferBalance(user1, user2, Amount);
            if (result != null)
                return result;
            else
                return null;
        }

        public DtoUser GetBalance(string PhoneNumber)
        {
            var result = _userRepository.FindUser(PhoneNumber);

            if (result != null)
            {
                return result;
            }
            else
                return null;
        }

        public DtoUser TopUp(DtoUser user, int Amount)
        {
            var result = _transferRepository.TopUp(user, Amount);

            if (result != null)
                return result;
            else
                return null;
        }

        public IList<Transfer> GetTransfers(string PhoneNumber)
        {
            var transfers = _transferRepository.GetTransactions(PhoneNumber);

            if (transfers != null)
                return transfers;
            else
                return null;
        }
    }
}
