﻿using Revolut.Model;
using System.Collections.Generic;
using Revolut.DtoModels;

namespace Revolut.Services
{
    public interface IUserService
    {

        CreateUserDto AddUser(User user);

        DtoUser UpdateUser(User user);

        DtoUser DeleteUser(string PhoneNumber);

        DtoUser FindUser(string PhoneNumber);

        bool CheckLogin(string PhoneNumber, string password);

    }
}
