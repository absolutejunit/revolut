﻿using Revolut.Model;
using System.Collections.Generic;
using Revolut.DtoModels;
using System;

namespace Revolut.Services
{
    public interface ITransferService
    {
        DtoUser TransferBalance(DtoUser user1, DtoUser user2, int amount);

        DtoUser GetBalance(string PhoneNumber);

        DtoUser TopUp(DtoUser user, int amount);

        IList<Transfer> GetTransfers(string PhoneNumber);


    }
}
