﻿using Revolut.Model;
using System.Collections.Generic;
using Revolut.DtoModels;

namespace Revolut.Services
{
    public interface IFriendGroupService
    {
        FriendGroupDTO CreateGroup(FriendGroup group);

        string DeleteGroup(string GroupName);

        IList<UsersGroupDTO> GetAllMembers(string GroupName);

        void AddPerson(string PhoneNumber, string GroupName);

        void RemovePerson(string PhoneNumber, string GroupName);

        FriendGroupDTO FindGroup(string GroupName);

        IList<FriendGroupDTO> GetAllGroups();

    }
}
