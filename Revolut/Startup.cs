﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Revolut.Repositories;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Revolut.Mapping;
using Revolut.Services;
using Revolut.Repositories;
using Swashbuckle;

namespace Revolut
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

            // This method gets called by the runtime. Use this method to add services to the container.
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
            public void ConfigureServices(IServiceCollection services)
            {

            services.AddSwaggerGen(c =>
           {
               c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
               {
                   Version = "v1",
                   Title = "Accesa Start API",
                   TermsOfService = "None"
               });
           });

                services.AddSingleton(Configuration);
                services.AddMvc();
                services.AddAutoMapper();

                //services.AddScoped<IUserService, UsersService>();
                //services.AddTransient<IUserService, UsersService>();
                services.AddScoped<IFriendGroupService, FriendGroupService>();
                services.AddScoped<ITransferService, TransferService>();
                services.AddScoped<IUserService, UsersService>();
                services.AddScoped<IUserMapping, UserMapping>();

                services.AddScoped<IUserRepository, UserRepository>();
                services.AddScoped<ITransferRepository, TransferRepository>();
                services.AddScoped<IFriendGroupRepository, FriendGroupRepository>();
        }
     

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseStatusCodePages();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
           {
               c.SwaggerEndpoint("/swagger/v1/swagger.json", "Start API v1");
           });
            /*app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });*/
        }
    }
}
