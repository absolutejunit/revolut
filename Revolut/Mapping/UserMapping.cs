﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Revolut.DtoModels;
using Revolut.Model;

namespace Revolut.Mapping
{
    public class UserMapping : IUserMapping
    {
        public DtoUser GetUserDtoFromUser(User user)
        {
            var dtoUser = new DtoUser()
            {
                PhoneNumber = user.PhoneNumber,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Balance = user.Balance
            };

            return dtoUser;
        }

        public User GetUserFromUserDTO(DtoUser dtoUser)
        {
            var user = new User()
            {
                PhoneNumber = dtoUser.PhoneNumber,
                FirstName = dtoUser.FirstName,
                LastName = dtoUser.LastName,
                Balance = dtoUser.Balance
            };

            return user;
        }

        public CreateUserDto GetCreateUserDtoFromUser(User user)
        {
            var dtoUser = new CreateUserDto()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Password =  user.Password,
                Balance = user.Balance
            };

            return dtoUser;
        }

        public User GetUserFromCreateUserDTO(CreateUserDto dtoUser)
        {
            var user = new User()
            {
                FirstName = dtoUser.FirstName,
                LastName = dtoUser.LastName,
                Password = dtoUser.Password,
                Balance = dtoUser.Balance
            };

            return user;
        }
    }
}
