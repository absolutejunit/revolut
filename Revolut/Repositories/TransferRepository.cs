﻿using Revolut.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Configuration;
using Revolut.Services;
using Revolut.DtoModels;
using System;

namespace Revolut.Repositories
{
    public class TransferRepository : ITransferRepository
    {
        private IUserService _userService;
        private string _connectionString;
        private DbConnection _dbConnection;
        private DateTime dateTime;

        public TransferRepository(IConfiguration configuration, IUserService userService)
        {
            _connectionString = configuration["connectionStrings:RevolutDB"];
            _dbConnection = new SqlConnection(_connectionString);
            _userService = userService;
        }

        public DtoUser TransferBalance(DtoUser user1, DtoUser user2, int Amount)
        {
            if (user1.Balance - Amount >= 0)
            {
                user1.Balance -= Amount;
                user2.Balance += Amount;
                var sql = "Update Users SET " +
                          "Balance = @Balance " +
                          "Where PhoneNumber = @PhoneNumber"
                          ;
                var sql2 = "Insert Into Transfers(PhoneNumber, TargetPhoneNumber, Amount, Date) " +
                           "Values(@PhoneNumber, @TargetPhoneNumber, @Amount, @Date)";

                _dbConnection.Query(sql, new { Balance = (int)user1.Balance, PhoneNumber = user1.PhoneNumber });
                _dbConnection.Query(sql, new { Balance = (int)user2.Balance, PhoneNumber = user2.PhoneNumber });

                _dbConnection.Query(sql2, new { PhoneNumber = user1.PhoneNumber, TargetPhoneNumber = user2.PhoneNumber, Amount = -Amount, Date = DateTime.Now.ToString("dd/MM/yyyy")});
                _dbConnection.Query(sql2, new { PhoneNumber = user2.PhoneNumber, TargetPhoneNumber = user1.PhoneNumber, Amount = Amount, Date = DateTime.Now.ToString("dd/MM/yyyy") });
                return user1;
            }
            else
                return null;
        }

        public DtoUser TopUp(DtoUser user1, int Amount)
        {

            if (user1 != null)
            {
                user1.Balance += Amount;

                var sql = "Update Users SET " +
                          "Balance = @Balance " +
                          "Where PhoneNumber = @PhoneNumber"
                          ;
                var sql2 = "Insert Into Transfers(PhoneNumber, TargetPhoneNumber, Amount, Date) " +
                           "Values(@PhoneNumber, @TargetPhoneNumber, @Amount, @Date)";

                _dbConnection.Query(sql, new { Balance = (int)user1.Balance, PhoneNumber = user1.PhoneNumber });
                _dbConnection.Query(sql2, new { PhoneNumber = user1.PhoneNumber, TargetPhoneNumber = user1.PhoneNumber, Amount = -Amount, Date = DateTime.Now.ToString("dd/MM/yyyy")});
                return user1;
            }
            else
                return null;
        }

        public IList<Transfer> GetTransactions(string PhoneNumber)
        {
            var sql = "SELECT * FROM Transfers " +
                      "WHERE Transfers.PhoneNumber = @PhoneNumber";

            var transfers = _dbConnection.Query<Transfer>(sql, new {  PhoneNumber = PhoneNumber }).ToList();

            return transfers;
        }

    }
}
