﻿using Revolut.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Configuration;
using Revolut.DtoModels;

namespace Revolut.Repositories
{
    public class UserRepository : IUserRepository
    {
        private string _connectionString;
        private DbConnection _dbConnection;

        public UserRepository(IConfiguration configuration)
        {
            _connectionString = configuration["connectionStrings:RevolutDB"];
            _dbConnection = new SqlConnection(_connectionString); 
        }

        public DtoUser FindUser(string PhoneNumber)
        {
            var sql = "SELECT * FROM Users WHERE PhoneNumber = @PhoneNumber";

            var user = _dbConnection.Query(sql, new { PhoneNumber = PhoneNumber }).FirstOrDefault();
            if (user == null)
                return null;
            else
                return new DtoUser { PhoneNumber = user.PhoneNumber, FirstName = user.FirstName, LastName = user.LastName, Balance = (int)user.Balance };
        }

        public CreateUserDto Add(CreateUserDto user)
        {
            
            var sql = "Insert Into Users(PhoneNumber, FirstName, LastName, Password, Balance) " +
                      "Values(@PhoneNumber, @FirstName, @LastName, @Password, @Balance)";
            _dbConnection.Query(sql, new { PhoneNumber=user.PhoneNumber, FirstName = user.FirstName, LastName = user.LastName, Password = user.Password, Balance = (int)user.Balance });
            return user;
        }

        public void Update(DtoUser user)
        {
            var sql = "Update Users SET" +
                      "PhoneNumber = @PhoneNumber" +
                      "FirstName = @FirstName" +
                      "LastName = @LastName" +
                      "Balance = @Balance" +
                      "Where PhoneNumber = @PhoneNumber"
                      ;

            _dbConnection.Query(sql,new {user = user});
        }

        public void Delete(string PhoneNumber)
        {
            var sql = "Delete From Users Where PhoneNumber = @PhoneNumber";

            _dbConnection.Query(sql,new { PhoneNumber = PhoneNumber });
        }
        public CreateUserDto CheckLogin(string PhoneNumber, string Password)
        {
            var sql = "SELECT * FROM Users " +
                      "WHERE(PhoneNumber = @PhoneNumber AND Password = @Password)"
                      ;

            var check = _dbConnection.Query(sql, new { PhoneNumber = PhoneNumber, Password = Password }).FirstOrDefault();

            if (check != null)
                return new CreateUserDto { PhoneNumber = check.PhoneNumber, FirstName = check.FirstName, LastName = check.LastName, Password = check.Password, Balance = (int)check.Balance };
            else
                return null;
        }
    }
}
