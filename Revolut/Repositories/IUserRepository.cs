﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Revolut.DtoModels;
using Revolut.Model;

namespace Revolut.Repositories
{
    public interface IUserRepository
    {
        DtoUser FindUser(string PhoneNumber);
        CreateUserDto Add(CreateUserDto user);
        void Update(DtoUser user);
        void Delete(string PhoneNumber);
        CreateUserDto CheckLogin(string PhoneNumber, string Password);
    }
}
