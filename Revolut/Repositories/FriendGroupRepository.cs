﻿using Revolut.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Configuration;
using Revolut.DtoModels;

namespace Revolut.Repositories
{
    public class FriendGroupRepository : IFriendGroupRepository
    {
        private string _connectionString;
        private DbConnection _dbConnection;

        public FriendGroupRepository(IConfiguration configuration)
        {
            _connectionString = configuration["connectionStrings:RevolutDB"];
            _dbConnection = new SqlConnection(_connectionString);
        }

        public FriendGroupDTO FindGroup(string GroupName)
        {
            var sql = "SELECT * FROM FriendGroups Where GroupName = @GroupName";
            var group = _dbConnection.Query(sql, new { GroupName = GroupName }).FirstOrDefault();

            if (group == null)
                return null;
            else
                return new FriendGroupDTO { GroupName = group.GroupName, OwnerPhoneNumber = group.OwnerPhoneNumber };
        }

        public IList<UsersGroupDTO> GetMembers(string GroupName)
        {
            var sql = "SELECT UserPhoneNumber FROM UsersGroup " +
                      "WHERE UsersGroup.GroupName = @GroupName";

            var group = _dbConnection.Query<UsersGroupDTO>(sql, new { GroupName = GroupName }).ToList();

            return group;
        }

        public FriendGroupDTO CreateGroup(FriendGroupDTO group)
        {
            var sql = "INSERT INTO FriendGroups(GroupName, OwnerPhoneNumber) " +
                      "VALUES(@GroupName, @OwnerPhoneNumber)";
            _dbConnection.Query(sql, new { GroupName = group.GroupName, OwnerPhoneNumber = group.OwnerPhoneNumber });

            return group;
                      
        }

        public string DeleteGroup(string GroupName)
        {
            var sql = "DELETE FROM FriendGroups WHERE GroupName = @GroupName";
            var homemadecascade = "DELETE FROM UsersGroup WHERE GroupName = @GroupName"; 

            _dbConnection.Query(sql, new { GroupName = GroupName });
            _dbConnection.Query(homemadecascade, new { GroupName = GroupName });

            return GroupName;
        }
        public void AddPerson(string PhoneNumber, string GroupName, bool exists)
        {
            var sql = "INSERT INTO UsersGroup(UserPhoneNumber, GroupName) " +
                      "VALUES(@PhoneNumber, @GroupName)";
            var cascade = "INSERT INTO FriendGroups(GroupName, OwnerPhoneNumber) " +
                          "VALUES(@GroupName, @PhoneNumber)";

            _dbConnection.Query(sql, new { PhoneNumber = PhoneNumber, GroupName = GroupName });
            if (!exists)
            {
                _dbConnection.Query(cascade, new { GroupName = GroupName, PhoneNumber = PhoneNumber });
            }

        }
        public void RemovePerson(string PhoneNumber, string GroupName)
        {
            var sql = "DELETE FROM UsersGroup WHERE (UserPhoneNumber = @PhoneNumber AND GroupName = @GroupName)";

            _dbConnection.Query(sql, new { PhoneNumber = PhoneNumber, GroupName = GroupName });

        }

        public IList<UsersGroupDTO> GetAllMembers(string GroupName)
        {
            var sql = "SELECT UserPhoneNumber FROM UsersGroup " +
                      "WHERE UsersGroup.GroupName = @GroupName";

            var members = _dbConnection.Query<UsersGroupDTO>(sql, new { GroupName = GroupName }).ToList();

            return members;
        }

        public IList<FriendGroupDTO> GetAllGroups()
        {
            var sql = "SELECT GroupName FROM FriendGroups";

            var groups = _dbConnection.Query<FriendGroupDTO>(sql).ToList();

            return groups;
        }
    }
}