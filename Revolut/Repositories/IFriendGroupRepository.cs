﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Revolut.DtoModels;
using Revolut.Model;

namespace Revolut.Repositories
{
    public interface IFriendGroupRepository
    {
        FriendGroupDTO FindGroup(string GroupName);

        FriendGroupDTO CreateGroup(FriendGroupDTO group);

        IList<UsersGroupDTO> GetMembers(string GroupName);

        string DeleteGroup(string GroupName);

        void AddPerson(string PhoneNumber, string GroupName, bool exists);

        void RemovePerson(string PhoneNumber, string GroupName);

        IList<UsersGroupDTO> GetAllMembers(string GroupName);

        IList<FriendGroupDTO> GetAllGroups();
    }
}
