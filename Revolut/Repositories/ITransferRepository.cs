﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Revolut.DtoModels;
using Revolut.Model;

namespace Revolut.Repositories
{
    public interface ITransferRepository
    {
        DtoUser TransferBalance(DtoUser user1, DtoUser user2, int Amount);

        DtoUser TopUp(DtoUser user1, int Amount);

        IList<Transfer> GetTransactions(string PhoneNumber);

    }
}
